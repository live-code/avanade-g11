import { Component } from '@angular/core';
import { ThemeService } from './core/services/theme.service';

@Component({
  selector: 'app-root',
  template: `
    <div 
      style="padding: 10px"
      [style.background-color]="themeService.theme === 'dark' ? '#222' : '#dedede'"
    >
      <button routerLink="home">home</button> 
      <button routerLink="contacts">contacts</button>
      <button routerLink="tvmaze">tvmaze</button>
      <button routerLink="users">users</button>
    </div>
  
    <hr>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  constructor(public themeService: ThemeService) {
  }
}
