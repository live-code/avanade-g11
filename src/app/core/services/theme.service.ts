import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private value = 'dark';

  set theme(theme: string) {
    // ...
    this.value = theme;
  }

  get theme(): string {
    return this.value;
  }
}
