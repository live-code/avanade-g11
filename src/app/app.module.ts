import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { registerLocaleData } from '@angular/common';
import it from '@angular/common/locales/it';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './features/users/users.component';
import { HomeComponent } from './features/home/home.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { RouterModule } from '@angular/router';
import { UserDetailsComponent } from './features/user-details/user-details.component';
import { routes } from './routes';
import { UsersService } from './features/users/services/users.service';
import { UserChildComponent } from './features/users/components/user-child.component';
import { TvmazeComponent } from './features/tvmaze/tvmaze.component';

@NgModule({
  declarations: [
    AppComponent, UsersComponent, HomeComponent, ContactsComponent, UserDetailsComponent, UserChildComponent, TvmazeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

registerLocaleData(it, 'it');
