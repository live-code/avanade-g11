import { HomeComponent } from './features/home/home.component';
import { UsersComponent } from './features/users/users.component';
import { UserDetailsComponent } from './features/user-details/user-details.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { TvmazeComponent } from './features/tvmaze/tvmaze.component';

export const routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'tvmaze',
    component: TvmazeComponent
  },
  {
    path: 'users/:id',
    component: UserDetailsComponent
  },
  {
    path: 'contacts',
    component: ContactsComponent
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];
