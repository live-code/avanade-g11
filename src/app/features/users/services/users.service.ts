import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from '../../../model/user';
import { NgForm } from '@angular/forms';

@Injectable()
export class UsersService {
  users: User[]
  error: boolean;
  activeUser: User;

  constructor(private http: HttpClient) {}

  getAll(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(
        result => this.users = result,
        err => {
          this.error = true;
        }
      );
  }

  deleteUser(id: number): void {
    this.error = false;
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe(
        () => this.users = this.users.filter(user => user.id !== id),
        () => this.error = true
      );
  }

  saveHandler(user: User): Promise<string | number> {
    if (this.activeUser) {
      return this.editHandler(user);
    } else {
      return this.addHandler(user);
    }
  }

  editHandler(user: User): Promise<string> {
    return new Promise((resolve, reject) => {
      this.error = false;
      this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${this.activeUser.id}`, user)
        .subscribe(
          result => {
            // const index = this.users.findIndex(user => user.id === this.activeUser.id);
            // this.users[index] = result;
            this.users = this.users.map(u => {
              return u.id === this.activeUser.id ? result : user;
            });

            resolve('edited');
          },
          (e) => {
            this.error = true;
            reject(e);
          }
        );
    });
  }

  addHandler(user: User): Promise<number> {
    return new Promise((resolve, reject) => {
      this.error = false;
      this.http.post<User>('https://jsonplaceholder.typicode.com/users', user)
        .subscribe(
          result => {
            this.users = [...this.users, result];
            resolve(123);
          },
          (e) => {
            this.error = true;
            reject(e);
          }
        );
    });
  }

  selectUser(user: User): void {
    this.activeUser = user;
  }

  clean(): void {
    this.activeUser = null;
  }

}
