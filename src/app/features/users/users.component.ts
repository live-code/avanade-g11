import { ChangeDetectionStrategy, Component } from '@angular/core';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users',
  template: `
    <app-user-child></app-user-child>
    <form
      #f="ngForm"
      (submit)="saveHandler(f)"
      style="padding: 10px; border: 3px solid black"
      [style.borderColor]="f.valid ? 'green' : 'red'"
    >
      <!--[ngFormOptions]="{ updateOn: 'blur' }"-->

      <small *ngIf="inputName.errors?.required">This field is required</small>
      <small *ngIf="inputName.errors?.minlength">
        Too Short. Missing {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} chars
      </small>

      <input
        type="text"
        name="name"
        placeholder="Name *"
        [ngModel]="userService.activeUser?.name"
        required minlength="5"
        class="form-control"
        [ngClass]="{
          'is-invalid': inputName.invalid,
          'is-valid': inputName.valid
        }"
        #inputName="ngModel"
      >
      <small *ngIf="inputUsername.errors?.required">This field is required</small>
      <small *ngIf="inputUsername.errors?.pattern">No symbols</small>

      <input
        type="text" name="username"
        [ngModel]="userService.activeUser?.username" 
        required pattern="[a-zA-Z ]*"
        [ngClass]="{
          'is-invalid': inputUsername.invalid && f.dirty,
          'is-valid': inputUsername.valid
        }"
        class="form-control" #inputUsername="ngModel">

      
      <div class="btn-group">
        <button
          class="btn"
          [ngClass]="{
            'btn-primary': f.valid,
            'btn-danger': f.invalid
          }"
          [disabled]="f.invalid"
          type="submit">
          {{userService.activeUser ? 'EDIT' : 'ADD'}}
        </button>
        
        <button
          class="btn btn-outline-primary"
          (click)="cleanHandler(f)"
          [disabled]="!userService.activeUser"
        >Clean</button>
      </div>
    </form>


    <!--error msg-->
    <div class="alert alert-danger" *ngIf="userService.error">errore</div>

    <!--user list-->
    <div class="container mt-3">
      <li
        *ngFor="let user of userService.users"
        class="list-group-item "
        [ngClass]="{
          active: user.id === userService.activeUser?.id
         }"
        (click)="userService.selectUser(user)"
      >
        {{user.name}} {{user.username}}

        <div class="pull-right">
          <i class="fa fa-info-circle" 
             [routerLink]="'/users/' + user.id"></i>
          <i class="fa fa-trash"
             (click)="deleteHandler(user.id, $event)"></i>
        </div>
      </li>
    </div>

    <pre>{{userService.activeUser | json}}</pre>
  `,
  providers: [
    UsersService
  ],
})
export class UsersComponent {
  constructor(
    public userService: UsersService,
  ) {
    userService.getAll();
  }

  cleanHandler(f: NgForm): void {
    f.reset();
    this.userService.clean();
  }

  saveHandler(form: NgForm): void {
    this.userService.saveHandler(form.value)
      .then((res) => {
        if (typeof res === 'string') {
          console.log('sono una stringa');
        } else {
          console.log('sono un number');
        }
      })
      .catch(e => console.log('err', e));
  }

  deleteHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.userService.deleteUser(id);

  }
}
