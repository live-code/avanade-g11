import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-user-child',
  template: `
    <p>
      user-child works! {{userService.users?.length}}
    </p>
  `,
  styles: [
  ]
})
export class UserChildComponent implements OnInit {

  constructor(public userService: UsersService) {}

  ngOnInit(): void {
  }

}
