import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-home',
  template: `
    <h1>Theme: {{themeService.theme}}</h1>
    
    <button (click)="themeService.theme = 'light'">light</button>
    <button (click)="themeService.theme = 'dark'">dark</button>
  `,
})
export class HomeComponent {
  constructor( public themeService: ThemeService ) {}
}
