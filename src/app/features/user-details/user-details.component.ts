import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  template: `
    <pre>{{user?.name}}</pre>
    <pre>{{user?.username}}</pre>
    <pre>{{user?.phone}}</pre>
    
    <button routerLink="/users">back to list</button>
  `,
  styles: [
  ]
})
export class UserDetailsComponent implements OnInit {
  user: User;

  constructor(http: HttpClient, activatedRoute: ActivatedRoute) {
    const id = activatedRoute.snapshot.params.id;
    http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .subscribe(result => {
        this.user = result;
      })
  }

  ngOnInit(): void {
  }

}
