import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Show, TvmazeResponse } from '../../model/tvmaze-response';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { User } from '../../model/user';

@Component({
  selector: 'app-tvmaze',
  templateUrl: './tvmaze.component.html',
  styleUrls: ['./tvmaze.component.css']
})
export class TvmazeComponent {
  result: TvmazeResponse[];
  input: FormControl = new FormControl('');
  show: Show;

  constructor(private http: HttpClient) {
    this.http.get<TvmazeResponse[]>(`http://api.tvmaze.com/search/shows?q=soprano`)
      .pipe(
        map(result => result.reduce((acc, curr) => acc + curr.score, 0)),
        map(total => total > 10 ? 'super bello' : 'mmmmm')
      )
      .subscribe(msg => {
        console.log(msg); // name (username)
      })


    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap(text => this.http.get<TvmazeResponse[]>(`http://api.tvmaze.com/search/shows?q=${text}`))
      )
      .subscribe(result => this.result = result)
  }

  showMOdal(show: Show): void {
    this.show = show;
  }
}
